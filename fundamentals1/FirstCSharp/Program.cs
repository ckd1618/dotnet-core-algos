﻿using System;

namespace FirstCSharp
{
    class Program
    {
        static void Main(string[] args) {

            string str;
            short i = 1;
            while (i < 101) {
                str = "";
                if (i%3 == 0) {
                    str = str + "Fizz";
                }
                if (i%5 == 0) {
                    str = str + "Buzz";
                }
                if (str.Length > 0) {
                    Console.WriteLine(str);
                }
                i++;
            }


            // string str;
            // for (short i = 1; i<101; i++) {
            //     str = "";
            //     if (i%3 == 0) {
            //         str = str + "Fizz";
            //     }
            //     if (i%5 == 0) {
            //         str = str + "Buzz";
            //     }
            //     if (str.Length > 0) {
            //         Console.WriteLine(str);
            //     }
            // }


            // for (short i = 1; i < 101; i++) {
            //     if ((i % 3 == 0 && i % 5 != 0) || (i % 3 != 0 && i % 5 == 0)) {
            //         Console.WriteLine(i);
            //     }
            // }


            // for (short i = 1; i < 256; i++)
            // {
            //     Console.WriteLine(i);
            // }

        }
    }
}
