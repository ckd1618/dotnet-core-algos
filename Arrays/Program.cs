﻿using System;
using System.Collections.Generic;//List, Dictionary

namespace Arrays {
    class Program {
        static void Main(string[] args) {
            // Console.WriteLine(string.Join(", ", args));

            var dic = new Dictionary<int,string>() {
                {4, "four"},
                {3, "three"},
                {6, "six"}
            };
            dic.Add(5, "five");
            Console.WriteLine(dic);
            Console.WriteLine(string.Join(", ", dic));

            foreach (KeyValuePair<int,string> each in dic) {
                Console.WriteLine(each);
            }
            
            

            // var lis = new List<int>{5,4,3,2,2};
            // lis.Insert(2, 55);
            // lis.RemoveAt(2);
            // lis.Add(3);
            // lis.AddRange(new List<int>{3,3,4,5});
            // Console.WriteLine(string.Join(", ", lis));

            // var lis = new List<int>{5,4,3,2,2};
            // var arr = new int[]{3,4,5,6};
            // lis.AddRange(arr);
            // Console.WriteLine(string.Join(", ", lis));
        }
    }
}
